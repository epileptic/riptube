package epileptic.odp.ytdl.core;

public class Constant {
	public static final class Downloader {
		public static final String[] DEFAULT_PARAMETER = { "--audio-format mp3", "-x", "--audio-quality 4",
				"--add-metadata" };

		public static final String COMMAND = "youtube-dl";
		public static final String MAX_DOWNLOADS = "--max-downloads";
		public static final String YES_PLAYLIST = "--yes-playlist";
		public static final String NO_PLAYLIST = "--no-playlist";
		public static final String CONFIG_LOCATION = "--config-location";

		public static final int DEFAULT_PLAYLIST_LIMITER = 15;

		public static final int TIMEOUT_PER_VIDEO_SECONDS = 30;
	}
}
