package epileptic.odp.ytdl.core;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.epileptic.eplib.log.Log;
import epileptic.odp.ytdl.downloader.Downloader;
import epileptic.odp.ytdl.downloader.PlaylistType;

public class YTDLController {

	private static ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
	private static ArrayList<Downloader> downloader = new ArrayList<Downloader>();

	public static void overwriteExecutorService(int threadCount) {
		pool = Executors.newFixedThreadPool(threadCount);
	}

	public static void startDownloads(PlaylistType pt, int maxVideos, List<String> urls, Path customConfigPath) {
		Downloader current;
		for (String u : urls) {
			try {
				current = new Downloader(new URL(u));
				current.setPlaylistBehavior(pt);

					current.maxVideos(maxVideos);

				if (customConfigPath != null) {
					current.setCustomConfig(customConfigPath);
					current.setIgnoreGeneratedConfig(true);
				}
				
				pool.execute(current);
				downloader.add(current);
			} catch (MalformedURLException e) {
				Log.logException(e);
			}
		}
	}

	public static boolean isWorking() {
		boolean working = false;
		for (Downloader d : downloader)
			if (!d.isDone()) {
				working = true;
				break;
			}

		return working;
	}
}
