package epileptic.odp.ytdl.core;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import de.epileptic.eplib.Registry;
import epileptic.odp.ytdl.ui.View;

public class Main {
	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			try {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
					| UnsupportedLookAndFeelException e2) {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e3) {
				}
			}
		}

		Registry.getInstance().setVerbose(true);
		System.out.println("https://icons8.com/icon/pack/files/small");

		View v = new View();
		v.show();
	}
}
