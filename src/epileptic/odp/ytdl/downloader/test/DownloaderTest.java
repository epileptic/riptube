package epileptic.odp.ytdl.downloader.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

import epileptic.odp.ytdl.downloader.Downloader;
import epileptic.odp.ytdl.downloader.PlaylistType;

class DownloaderTest {

	@Test
	void testIgnoreGeneratedConfig() {
		String url = "https://www.youtube.com/watch?v=uOzPqiRpumc&list=RDcZzx5H2wkds";
		try {
			Downloader dw = new Downloader(new URL(url));

			dw.setIgnoreGeneratedConfig(true);

			assertEquals(dw.dryStart(), "youtube-dl https://www.youtube.com/watch?v=uOzPqiRpumc&list=RDcZzx5H2wkds");
		} catch (MalformedURLException e) {
			fail(e);
		}
	}

	@Test
	void testSetCustomConfig() {
		String url = "https://www.youtube.com/watch?v=uOzPqiRpumc&list=RDcZzx5H2wkds";
		try {
			Downloader dw = new Downloader(new URL(url));

			dw.setCustomConfig(Paths.get("/home/epileptic/Documents/youtube-dl.config"));
			dw.setIgnoreGeneratedConfig(true);

			assertEquals(dw.dryStart(),
					"youtube-dl --config-location /home/epileptic/Documents/youtube-dl.config https://www.youtube.com/watch?v=uOzPqiRpumc&list=RDcZzx5H2wkds");
		} catch (MalformedURLException e) {
			fail(e);
		}
	}

	@Test
	void testPlaylistBehavior() {
		String url = "https://www.youtube.com/watch?v=uOzPqiRpumc&list=RDcZzx5H2wkds";
		try {
			Downloader dw = new Downloader(new URL(url));

			dw.setPlaylistBehavior(PlaylistType.AUTO);
			assertEquals(dw.dryStart(),
					"youtube-dl --audio-format mp3 -x --audio-quality 6 --add-metadata --max-downloads 15 https://www.youtube.com/watch?v=uOzPqiRpumc&list=RDcZzx5H2wkds");

			dw.setPlaylistBehavior(PlaylistType.YES_PLAYLIST);
			assertEquals(dw.dryStart(),
					"youtube-dl --audio-format mp3 -x --audio-quality 6 --add-metadata --yes-playlist --max-downloads 15 https://www.youtube.com/watch?v=uOzPqiRpumc&list=RDcZzx5H2wkds");

			dw.setPlaylistBehavior(PlaylistType.NO_PLAYLIST);
			assertEquals(dw.dryStart(),
					"youtube-dl --audio-format mp3 -x --audio-quality 6 --add-metadata --no-playlist --max-downloads 15 https://www.youtube.com/watch?v=uOzPqiRpumc&list=RDcZzx5H2wkds");
		} catch (MalformedURLException e) {
			fail(e);
		}
	}

	@Test
	void testPlaylistLimiter() {
		String url = "https://www.youtube.com/watch?v=uOzPqiRpumc&list=RDcZzx5H2wkds";
		try {
			Downloader dw = new Downloader(new URL(url));

			dw.maxVideos(30);
			
			assertEquals(dw.dryStart(),
					"youtube-dl --audio-format mp3 -x --audio-quality 6 --add-metadata --max-downloads 30 https://www.youtube.com/watch?v=uOzPqiRpumc&list=RDcZzx5H2wkds");
		} catch (MalformedURLException e) {
			fail(e);
		}
	}

}
