package epileptic.odp.ytdl.downloader;

import static epileptic.odp.ytdl.core.Constant.Downloader.COMMAND;
import static epileptic.odp.ytdl.core.Constant.Downloader.DEFAULT_PARAMETER;
import static epileptic.odp.ytdl.core.Constant.Downloader.DEFAULT_PLAYLIST_LIMITER;
import static epileptic.odp.ytdl.core.Constant.Downloader.MAX_DOWNLOADS;
import static epileptic.odp.ytdl.core.Constant.Downloader.NO_PLAYLIST;
import static epileptic.odp.ytdl.core.Constant.Downloader.TIMEOUT_PER_VIDEO_SECONDS;
import static epileptic.odp.ytdl.core.Constant.Downloader.*;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.concurrent.TimeoutException;

import de.epileptic.eplib.log.Log;

public class Downloader extends Thread {

	// Build
	private boolean ignoreGeneratedConfig = false;
	private StringBuilder command;

	// Parameter
	private URL videoUrl;

	private Path customConfig;
	private PlaylistType playlistBehavior = PlaylistType.AUTO;
	private int maxVideos = DEFAULT_PLAYLIST_LIMITER;

	// Runtime
	boolean isDone = false;

	/**
	 * 
	 * @param videoUrl URL to video, mix or playlist that has to be downloaded.
	 */
	public Downloader(URL videoUrl) {
		this.videoUrl = videoUrl;
	}

	/**
	 * Returns max amount of videos downloaded. Value used for youtube-dl parameter
	 * <code>--max-downloads</code>
	 * 
	 * @return max amount of videos downloaded.
	 */
	public int getPlaylistLimiter() {
		return maxVideos;
	}

	/**
	 * Sets max amount of videos downloaded. Value used for youtube-dl parameter
	 * <code>--max-downloads</code>
	 * 
	 * @param maxVideos max amount of videos to be downloaded.
	 */
	public void maxVideos(int maxVideos) {
		this.maxVideos = maxVideos;
	}

	/**
	 * 
	 * @return {@link URL} to video that has to be dowloaded.
	 */
	public URL getVideoUrl() {
		return videoUrl;
	}

	/**
	 * Overwrites video {@link URL}.
	 * 
	 * @param videoUrl set video URL of the video that has to be downloaded.
	 */
	public void setVideoUrl(URL videoUrl) {
		this.videoUrl = videoUrl;
	}

	/**
	 * Defines how URL will be interpreted. <code>YES_PLAYLIST</code> will force to
	 * download a mix or playlist. If URL is video it will use the autoplay mix.
	 * <code>NO_PLAYLIST</code> will always download the first video of a mix or
	 * playlist. <code>AUTO</code> will determine automatically. F
	 */
	public PlaylistType getPlaylistBehavior() {
		return playlistBehavior;
	}

	/**
	 * Defines how URL will be interpreted. <code>YES_PLAYLIST</code> will force to
	 * download a mix or playlist. If URL is video it will use the autoplay mix.
	 * <code>NO_PLAYLIST</code> will always download the first video of a mix or
	 * playlist. <code>AUTO</code> will determine automatically.
	 */
	public void setPlaylistBehavior(PlaylistType playlistBehavior) {
		this.playlistBehavior = playlistBehavior;
	}

	/**
	 * Custom config file for youtube-dl parameter <code>--config-location</code>.
	 * Refer to <a href=
	 * "https://github.com/ytdl-org/youtube-dl/blob/master/README.md#readme">documentation#options</a>
	 * for further information.
	 * 
	 * @return custom config path
	 */
	public Path getCustomConfig() {
		return customConfig;
	}

	/**
	 * Sets custom config file for youtube-dl parameter
	 * <code>--config-location</code>. Refer to <a href=
	 * "https://github.com/ytdl-org/youtube-dl/blob/master/README.md#readme">documentation#options</a>
	 * for further information.
	 * 
	 * @param customConfig {@link Path} to file
	 */
	public void setCustomConfig(Path customConfig) {
		this.customConfig = customConfig;
	}

	/**
	 * In case of a custom configuration file you maybe want to not generate default
	 * parameters. This disables the generation of those. Configuration file
	 * parameter will still be generated.
	 * 
	 * @return if configuration is suppressed
	 */
	public boolean isIgnoreGeneratedConfig() {
		return ignoreGeneratedConfig;
	}

	/**
	 * In case of a custom configuration file you maybe want to not generate default
	 * parameters. This disables the generation of those. Configuration file
	 * parameter will still be generated.
	 * 
	 */
	public void setIgnoreGeneratedConfig(boolean ignoreGeneratedConfig) {
		this.ignoreGeneratedConfig = ignoreGeneratedConfig;
	}

	/**
	 * If playlistLimiter is -1, no limit will be set
	 * 
	 * @param videoUrl
	 * @param type
	 * @param maxVideos
	 */
	private void build() {
		command = new StringBuilder(COMMAND);

		if (!ignoreGeneratedConfig) {
			// Append basic parameters
			for (String s : DEFAULT_PARAMETER)
				appendParameter(s);

			// Set playlist mode
			switch (playlistBehavior) {
			case YES_PLAYLIST:
				appendParameter(YES_PLAYLIST);
				break;

			case NO_PLAYLIST:
				appendParameter(NO_PLAYLIST);
				break;

			default:
				break;
			}

			// Max downloads
			if (maxVideos != -1) {
				appendParameter(MAX_DOWNLOADS);
				appendParameter(Integer.toString(maxVideos));
			}
		}

		if (customConfig != null) {
			appendParameter(CONFIG_LOCATION);
			appendParameter(customConfig.toAbsolutePath().toString());
		}

		// Add video url
		appendParameter(videoUrl.toString());
	}

	/**
	 * @return true if done; false otherwise
	 */
	public boolean isDone() {
		return isDone;
	}

	public String dryStart() {
		build();
		return command.toString();
	}

	@Override
	public void run() {

		// Build youtube-dl command
		build();

		try {
			// Run youtube-dl command
			Log.log("Starting process with command " + command.toString(), "Thread:" + this.getId());
			Process p = Runtime.getRuntime().exec(command.toString());

			// Wait for process to end
			while (p.isAlive()) {

				// Check for timeout
				int timeout = Math.abs(TIMEOUT_PER_VIDEO_SECONDS * maxVideos + 2);
				Log.log("Timeout set to " + timeout + " seconds!",
						this.getClass().getSimpleName() + ":" + this.getId());
				for (int i = 0; i < timeout && p.isAlive(); i++) {
					Thread.sleep(1000);
				}

				// Kill on timeout
				if (p.isAlive()) {
					Log.logException(new TimeoutException("Timeout reached. Killing thread..."));
					p.destroy();
					this.interrupt();
					break;
				}
			}

			Log.log("Download succesfull", this.getClass().getSimpleName() + ":" + this.getId());
		} catch (IOException | InterruptedException e) {
			Log.logWarn("Download failed. Further information: ", this);
			Log.logException(e);
		}

		isDone = true;
	}

	private void appendParameter(String param) {
		command.append(" " + param);
	}

}
