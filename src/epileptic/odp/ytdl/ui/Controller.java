package epileptic.odp.ytdl.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Controller implements ActionListener, KeyListener, ChangeListener, MouseListener {

	private JList<String> urls;
	private DefaultListModel<String> urlsListModel;
	private JButton addUrlButton;
	private JButton removeUrlButton;

	public Controller(JList<String> urls, DefaultListModel<String> urlsListModel, JButton addUrlButton,
			JButton removeUrlButton) {
		this.urls = urls;
		this.urlsListModel = urlsListModel;
		this.addUrlButton = addUrlButton;
		this.removeUrlButton = removeUrlButton;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
		for (Object s : urls.getSelectedValuesList())
			urlsListModel.removeElement(s);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == removeUrlButton) {
			for (Object s : urls.getSelectedValuesList())
				urlsListModel.removeElement(s);
		} else if (e.getSource() == addUrlButton) {

		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

}
