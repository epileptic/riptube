package epileptic.odp.ytdl.ui;

public class UIComponents {
	public static final class Buttons {
		public static final String ADD_URL = "addurl";
		public static final String REMOVE_URL = "removeurl";
		public static final String OPEN_FILE_EXPLORER = "openfileexplorer";
		public static final String CLEAR_PATH = "clearpath";
		public static final String CONVERT = "convert";
	}

	public static final class Radios {
		public static final String AUTO_PLAYLIST = "autoplaylist";
		public static final String YES_PLAYLIST = "yesplaylist";
		public static final String NO_PLAYLIST = "noplaylist";
	}

	public static final class TextFields {
		public static final String URL_FIELD = "urlinput";
		public static final String MAX_VIDEOS = "maxvideos";
		public static final String CONFIG_PATH = "configpathinput";
	}

	public static final class Misc {
		public static final String ALL_VIDEOS = "allvideos";
		public static final String URL_LIST = "urllist";
	}
}
