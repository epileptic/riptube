package epileptic.odp.ytdl.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import epileptic.odp.ytdl.core.YTDLController;
import epileptic.odp.ytdl.downloader.PlaylistType;

public class View implements ActionListener, KeyListener, ChangeListener, MouseListener {

	JFrame mainFrame = new JFrame();

	private JList<String> urls;

	private JRadioButton autoPlaylist;

	private JRadioButton yesPlaylist;

	private JRadioButton noPlaylist;

	private JButton removeLinkButton;

	private JButton addLinkButton;

	private JTextField urlInput;

	private JSpinner maxVideosSelection;

	private JCheckBox maxVideosWholePlaylist;

	private JTextField configFilePath;

	private JButton configFilePathSelect;

	private JButton configPathClear;

	private JButton start;

	private DefaultListModel<String> urlsListModel;

	private ButtonGroup playlistRadioButtons;

	private JProgressBar progress;

	public View() {
		mainFrame = new JFrame();
		mainFrame.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}

			@Override
			public void windowClosing(WindowEvent e) {
				if (YTDLController.isWorking()) {
					int result = JOptionPane.showConfirmDialog(mainFrame,
							"youtube-dl is still runnning! Really want to close?", "Still working!",
							JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null);
					if (result == 0)
						System.exit(0);
				} else {
					System.exit(0);
				}
			}

			@Override
			public void windowClosed(WindowEvent e) {
			}

			@Override
			public void windowActivated(WindowEvent e) {
			}
		});
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.setLayout(new GridBagLayout());
		mainFrame.setTitle("Riptube");
		mainFrame.setIconImage(new ImageIcon(ClassLoader.getSystemResource("assets/logo.png")).getImage());

		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(6, 6, 6, 6);

		// URL input field
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0.1;
		urlInput = new JTextField(64);
		urlInput.setName(UIComponents.TextFields.URL_FIELD);
		urlInput.addActionListener(this);
		urlInput.addKeyListener(this);
		mainFrame.add(urlInput, c);

		// Add URL button
		c.gridx = 1;
		c.weightx = 0;
		JPanel listControl = new JPanel();
		listControl.setLayout(new BoxLayout(listControl, BoxLayout.X_AXIS));

		addLinkButton = new JButton("Add URL");
		addLinkButton.setName(UIComponents.Buttons.ADD_URL);
		addLinkButton.addActionListener(this);
		listControl.add(addLinkButton, c);

		listControl.add(Box.createHorizontalStrut(6));

		removeLinkButton = new JButton("Remove URL");
		removeLinkButton.setName(UIComponents.Buttons.REMOVE_URL);
		removeLinkButton.addActionListener(this);
		listControl.add(removeLinkButton, c);

		mainFrame.add(listControl, c);

		// URL list
		c.gridy = 1;
		c.gridx = 0;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.BOTH;
		c.weighty = 1;
		urlsListModel = new DefaultListModel<String>();
		urls = new JList<String>(urlsListModel);

		urls.setName(UIComponents.Misc.URL_LIST);
		urls.addKeyListener(this);
		urls.addMouseListener(this);
		JScrollPane urlsScrollPane = new JScrollPane(urls);
		urlsScrollPane.setSize(urlsScrollPane.getWidth(), 400);
		mainFrame.add(urlsScrollPane, c);

		// PLAYLIST MODE
		c.gridy = 2;
		c.gridwidth = 2;
		c.weighty = 0;
		c.fill = GridBagConstraints.NONE;

		JLabel playlistDescription = new JLabel("Download as playlists (mixes, radios)");
		mainFrame.add(playlistDescription, c);

		c.gridy = 3;
		c.gridx = 0;
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		autoPlaylist = new JRadioButton("AUTO");
		autoPlaylist.setName(UIComponents.Radios.AUTO_PLAYLIST);
		autoPlaylist.addActionListener(this);
		buttonPanel.add(autoPlaylist);

		yesPlaylist = new JRadioButton("YES");
		yesPlaylist.setName(UIComponents.Radios.YES_PLAYLIST);
		yesPlaylist.addActionListener(this);
		buttonPanel.add(yesPlaylist);

		noPlaylist = new JRadioButton("NO");
		noPlaylist.setName(UIComponents.Radios.NO_PLAYLIST);
		noPlaylist.addActionListener(this);
		buttonPanel.add(noPlaylist);

		playlistRadioButtons = new ButtonGroup();
		playlistRadioButtons.add(autoPlaylist);
		playlistRadioButtons.add(yesPlaylist);
		playlistRadioButtons.add(noPlaylist);
		playlistRadioButtons.setSelected(autoPlaylist.getModel(), true);

		mainFrame.add(buttonPanel, c);

		// SEPERATOR
		c.gridy = 4;
		c.fill = GridBagConstraints.HORIZONTAL;
		mainFrame.add(new JSeparator(JSeparator.HORIZONTAL), c);

		// MAX VIDEOS
		c.gridy = 5;
		c.fill = GridBagConstraints.NONE;
		JLabel maxVideosDescription = new JLabel("Max amount of videos from one url (playlist, mix, radio)");
		mainFrame.add(maxVideosDescription, c);

		c.gridy = 6;
		JPanel maxVideosPanel = new JPanel();
		maxVideosPanel.setLayout(new BoxLayout(maxVideosPanel, BoxLayout.X_AXIS));

		maxVideosSelection = new JSpinner(new SpinnerNumberModel(15, -1, 1000, 1));
		maxVideosSelection.setName(UIComponents.TextFields.MAX_VIDEOS);
		maxVideosSelection.addChangeListener(this);
		maxVideosPanel.add(maxVideosSelection);

		maxVideosWholePlaylist = new JCheckBox("All (NOT RECOMMENDED)");
		maxVideosWholePlaylist.setName(UIComponents.Misc.ALL_VIDEOS);
		maxVideosWholePlaylist.addActionListener(this);
		maxVideosPanel.add(maxVideosWholePlaylist);

		mainFrame.add(maxVideosPanel, c);

		// SEPERATOR
		c.gridy = 7;
		c.fill = GridBagConstraints.HORIZONTAL;
		mainFrame.add(new JSeparator(JSeparator.HORIZONTAL), c);

		// CUSTOM CONFIG
		c.gridy = 8;
		c.fill = GridBagConstraints.NONE;

		JLabel openConfigDescription = new JLabel("Set custom config for youtube-dl");
		mainFrame.add(openConfigDescription, c);

		c.gridy = 9;
		JPanel openConfigPanel = new JPanel();
		openConfigPanel.setLayout(new BoxLayout(openConfigPanel, BoxLayout.X_AXIS));

		configFilePath = new JTextField(32);
		configFilePath.setName(UIComponents.TextFields.CONFIG_PATH);
		openConfigPanel.add(configFilePath);

		openConfigPanel.add(Box.createHorizontalStrut(6));

		configFilePathSelect = new JButton();
		configFilePathSelect.setName(UIComponents.Buttons.OPEN_FILE_EXPLORER);
		configFilePathSelect.addActionListener(this);
		configFilePathSelect.setIcon(new ImageIcon(ClassLoader.getSystemResource("assets/files.png")));
		openConfigPanel.add(configFilePathSelect);

		openConfigPanel.add(Box.createHorizontalStrut(6));

		configPathClear = new JButton("Clear");
		configPathClear.setName(UIComponents.Buttons.CLEAR_PATH);
		configPathClear.addActionListener(this);
		openConfigPanel.add(configPathClear);

		mainFrame.add(openConfigPanel, c);

		// BOTTOM BAR
		c.gridy = 10;
		c.fill = GridBagConstraints.HORIZONTAL;
		JPanel bottomBar = new JPanel();
		bottomBar.setLayout(new BoxLayout(bottomBar, BoxLayout.X_AXIS));

		// PROGRESSBAR
		progress = new JProgressBar();
		progress.setValue(0);
		progress.setStringPainted(true);
		progress.setString("idle");
		bottomBar.add(progress);

		bottomBar.add(Box.createHorizontalStrut(6));

		// START CONVERSION
		start = new JButton("Download and Convert");
		start.setName(UIComponents.Buttons.CONVERT);
		start.addActionListener(this);
		bottomBar.add(start);

		mainFrame.add(bottomBar, c);
	}

	public void show() {
		mainFrame.pack();
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getSource() == urlInput) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				addURL();
			}
		} else if (e.getSource() == urls) {
			if (e.getKeyCode() == KeyEvent.VK_DELETE) {
				removeURL();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addLinkButton) {
			addURL();
		} else if (e.getSource() == removeLinkButton) {
			removeURL();
		} else if (e.getSource() == start) {
			if (!urlInput.getText().equals(""))
				addURL();

			PlaylistType type = PlaylistType.AUTO;
			if (yesPlaylist.isSelected())
				type = PlaylistType.YES_PLAYLIST;
			else if (noPlaylist.isSelected())
				type = PlaylistType.NO_PLAYLIST;

			ArrayList<String> urlsString = new ArrayList<String>();
			Enumeration<String> urlsEnum = urlsListModel.elements();
			while (urlsEnum.hasMoreElements())
				urlsString.add(urlsEnum.nextElement());

			String configFileInputValue = configFilePath.getText();
			Path configFile = null;
			if (!configFileInputValue.equals("") && Files.exists(Paths.get(configFileInputValue)))
				configFile = Paths.get(configFileInputValue);

			if (maxVideosWholePlaylist.isSelected())
				maxVideosSelection.setValue(-1);

			YTDLController.startDownloads(type, (Integer) maxVideosSelection.getValue(), urlsString, configFile);

			new Thread(new Runnable() {

				@Override
				public void run() {
					mainFrame.setEnabled(false);
					while (YTDLController.isWorking()) {
						progress.setString("youtube-dl running...");
						progress.setIndeterminate(true);
					}
					progress.setString("idle");
					progress.setIndeterminate(false);
					progress.setValue(100);
					mainFrame.setEnabled(true);
				}
			}).start();

		} else if (e.getSource() == configFilePathSelect) {
			JFileChooser chooser = new JFileChooser(System.getProperty("user.home"));
			chooser.setMultiSelectionEnabled(false);
			int retval = chooser.showOpenDialog(mainFrame);
			if (retval == JFileChooser.APPROVE_OPTION)
				configFilePath.setText(chooser.getSelectedFile().toString());
		} else if (e.getSource() == configPathClear) {
			configFilePath.setText("");
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	private void addURL() {
		try {
			String input = urlInput.getText();
			new URL(input);
			if (!urlsListModel.contains(input))
				urlsListModel.addElement(input);
		} catch (MalformedURLException e1) {
			String[] buttons = { "Ok" };
			JOptionPane.showOptionDialog(null, "Invalid URL", "Invalid Input", JOptionPane.WARNING_MESSAGE, 0, null,
					buttons, buttons[0]);
		}
		urlInput.setText("");
	}

	private void removeURL() {
		for (String s : urls.getSelectedValuesList())
			urlsListModel.removeElement(s);
	}
}
